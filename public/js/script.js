(function(window, undefined) {

    window.RTCPeerConnection = window.RTCPeerConnection || window.webkitRTCPeerConnection || window.mozRTCPeerConnection;
    window.RTCIceCandidate = window.RTCIceCandidate || window.mozRTCIceCandidate || window.webkitRTCIceCandidate;
    window.RTCSessionDescription = window.RTCSessionDescription || window.mozRTCSessionDescription || window.webkitRTCSessionDescription;
    window.URL = window.URL || window.mozURL || window.webkitURL;
    window.navigator.getUserMedia = window.navigator.getUserMedia || window.navigator.webkitGetUserMedia || window.navigator.mozGetUserMedia;    
    
    var document = window.document;
    var $ = window.jQuery;

    var socket = null, socketId = null, others = [];
    
    var roomId = window.location.pathname.replace('/room/', '');
    var connections = {};
    var localStream = null;

    var CALL_TYPE_VIDEO = 'video', CALL_TYPE_AUDIO = 'audio', ONGOING_CALL = null;

    $(function() {

        var $actions = $('.actions');
        var $btnVideoCall = $('#btnVideoCall');
        var $btnAudioCall = $('#btnAudioCall');
        var $btnHangup = $('#btnHangup');

        socket = io();
        socket.on('hello', function(data) {
            socketId = data.id;
        });
        socket.on('join:result', function(data) {
            if(!data.success) {
                alert(data.error);
                window.location.href = '/';
            } else {
                others = data.others;
                if(others.length > 0) {
                    if(data.status === 'idle') {
                        onReadyToCall();
                    } else if(data.status === CALL_TYPE_VIDEO) {
                        onIncomingVideoCall();
                    } else if(data.status === CALL_TYPE_AUDIO) {
                        onIncomingAudioCall();
                    }
                }
            }
        });
        socket.on('room:join', function(data) {
            others.push(data.client);
            notify('<b>' + data.client.name + '</b> just joined.');
            if(others.length === 1) {
                onReadyToCall();
            }
        });
        socket.on('room:leave', function(data) {
            var index = others.findIndex(function(element, i, array) {
                return element.id === data.id;
            });
            if(index >= 0) {
                notify('<b>' + others[index].name + '</b> just left.');
                others.splice(index, 1);
            }
            if(others.length < 1) {
                onEmptyRoom();
            }
        });
        socket.on('candidate', function(fromId, candidate) {
            if(!connections[fromId]) initConnection(fromId);
            connections[fromId].addIceCandidate(new RTCIceCandidate(candidate));
        });
        socket.on('signal', function(fromId, data) {
            if(!connections[fromId]) initConnection(fromId);
            if(data.type === 'offer') {
                var desc = new RTCSessionDescription(data);
                connections[fromId].setRemoteDescription(desc);
                connections[fromId].createAnswer().then(function(sdp) {
                    connections[fromId].setLocalDescription(new RTCSessionDescription(sdp));
                    socket.emit('signal', fromId, sdp);
                });
            } else if(data.type === 'answer') {
                connections[fromId].setRemoteDescription(new RTCSessionDescription(data));
            }
        });
        socket.on('call:initiated', function(fromId, type) {
            notify('<b>' + findOther(fromId).name + '</b> started ' + type + ' call.');
            if(type === CALL_TYPE_VIDEO) {
                onIncomingVideoCall()
            } else if(type === CALL_TYPE_AUDIO) {
                onIncomingAudioCall();
            }
        });
        socket.on('call:accepted', function(fromId) {
            if(!ONGOING_CALL) return false;
            notify('<b>' + findOther(fromId).name + '</b> joined call.');
            if(!connections[fromId]) initConnection(fromId);
            connections[fromId].createOffer().then(function(desc) {
                connections[fromId].setLocalDescription(new RTCSessionDescription(desc));
                socket.emit('signal', fromId, desc);
            });
        });
        socket.on('call:hangup', function() {
            onHangup();
        });

        $btnVideoCall.on('click', function(event) {
            var $this = $(this);
            if($this.hasClass('success')) {
                // Incoming video call
                getMedia(function() {
                    socket.emit('call:accepted');
                    onOngoingCall();
                }, true);
            } else {
                // Outgoing video call
                getMedia(function() {
                    socket.emit('call:initiated', CALL_TYPE_VIDEO);
                    onOutgoingCall();
                }, true);
            }
            event.preventDefault();
            return false;
        });

        $btnAudioCall.on('click', function(event) {
            var $this = $(this);
            if($this.hasClass('success')) {
                // Incoming audio call
                getMedia(function() {
                    socket.emit('call:accepted');
                    onOngoingCall();
                }, false);
            } else {
                // Outgoing audio call
                getMedia(function() {
                    socket.emit('call:initiated', CALL_TYPE_AUDIO);
                    onOutgoingCall();
                }, false);
            }
            event.preventDefault();
            return false;
        });

        $btnHangup.on('click', function(event) {
            socket.emit('call:hangup');
            onHangup();
            event.preventDefault();
            return false;
        });

        $('#form-user-name').on('submit', function(event) {
            var $input = $(this).find('input#name');
            $input.removeClass('error');
            var name = $input.val().trim();
            if(name.length < 1) {
                $input.addClass('error').trigger('focus');
            } else {
                joinRoom(name);
                $(this).trigger('reset');
                $('.overlay').hide();
            }
            event.preventDefault();
            return false;
        });

        function joinRoom(name) {
            socket.emit('join', { roomId: roomId, id: socketId, name: name });
        }

        function initConnection(remoteId) {
            connections[remoteId] = new RTCPeerConnection({
                'iceServers': [
                    { 'url': 'stun:23.21.150.121' },
                    { 'url': 'stun:stun.l.google.com:19302' }
                ]
            }, {
                'optional': [
                    { 'DtlsSrtpKeyAgreement': true }
                ]
            });
            connections[remoteId].onicecandidate = function(event) {
                if(!event || !event.candidate) {
                    return false;
                }
                socket.emit('candidate', remoteId, event.candidate);
            };
            connections[remoteId].onaddstream = function(event) {
                setRemoteStream(remoteId, event.stream);
            };
            if(localStream) {
                connections[remoteId].addStream(localStream);
            }
        }

        function resetConnection() {
            for(conn in connections) {
                connections[conn].close();
                delete connections[conn];
            }
            $('video.remote, audio.remote').remove();
            $('video#local, audio#local').remove();
        }

        function onReadyToCall() {
            $actions.show();
            $btnVideoCall.show().removeClass('success');
            $btnAudioCall.show().removeClass('success');
            $btnHangup.hide();
        }

        function onEmptyRoom() {
            $actions.hide();
        }

        function onOutgoingCall() {
            $actions.show();
            $btnVideoCall.hide();
            $btnAudioCall.hide();
            $btnHangup.show().addClass('danger');
        }

        function onIncomingVideoCall() {
            $actions.show();
            $btnVideoCall.show().addClass('success');
            $btnAudioCall.hide();
            $btnHangup.show().addClass('danger');
        }

        function onIncomingAudioCall() {
            $actions.show();
            $btnVideoCall.hide();
            $btnAudioCall.show().addClass('success');
            $btnHangup.show().addClass('danger');
        }

        function onOngoingCall() {
            $actions.show();
            $btnVideoCall.hide();
            $btnAudioCall.hide();
            $btnHangup.show().addClass('danger');
        }

        function onHangup() {
            resetConnection();
            ONGOING_CALL = null;
            if(localStream) {
                stopStreaming(localStream);
            }
            onReadyToCall();
        }

        function getMedia(callback, isVideo) {
            isVideo = isVideo || false;
            navigator.getUserMedia({ audio: true, video: isVideo }, function(stream) {
                ONGOING_CALL = isVideo ? CALL_TYPE_VIDEO : CALL_TYPE_AUDIO;
                localStream = stream;
                setLocalStream();
                callback();
            }, function(err) {
                if(err.name === 'PermissionDeniedError') {
                    // Denied
                }
            });
        }

        function setLocalStream() {
            $('video#local, audio#local').remove();
            if(!ONGOING_CALL) return false;
            $('<' + ONGOING_CALL + ' />').attr('id', 'local').attr('autoplay', 'autoplay').attr('muted', 'muted').appendTo('body');
            if(window.URL) {
                $(ONGOING_CALL + '#local').attr('src', window.URL.createObjectURL(localStream));
            } else {
                $(ONGOING_CALL + '#local').attr('src', localStream);
            }
        }

        function setRemoteStream(remoteId, stream) {
            if(!ONGOING_CALL) return false;
            if($(ONGOING_CALL + '#stream_' + remoteId).length <= 0) {
                $('<' + ONGOING_CALL + ' />').attr('id', 'stream_' + remoteId).addClass('remote').attr('autoplay', 'autoplay').appendTo('body');
            }
            if(window.URL) {
                $(ONGOING_CALL + '#stream_' + remoteId).attr('src', window.URL.createObjectURL(stream));
            } else {
                $(ONGOING_CALL + '#stream_' + remoteId).attr('src', stream);
            }
        }

        function stopStreaming(stream) {
            stream.getAudioTracks().forEach(function(track) {
                track.stop();
            });
            stream.getVideoTracks().forEach(function(track) {
                track.stop();
            });
        }

        function notify(msg) {
            var $notification = $('<div></div>').addClass('notification').html(msg)
            $notification.appendTo('.notifications').delay(3000).fadeOut('slow', function() {
                $(this).remove();
            });
        }

        function findOther(id) {
            var other = others.find(function(element, index, array) {
                return element.id === id;
            });
            return other ? other : null;
        }

        $(window).on('beforeunload', function() {
            socket.emit('bye');
        });

    });

})(window);
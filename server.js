require('dotenv').config();

const express = require('express');
const app = express();
const http = require('http');
const https = require('https');
const path = require('path');
const fs = require('fs');
const httpPort = process.env.HTTP_PORT || 3000;
const httpsPort = process.env.HTTPS_PORT || 3001;
const hostIp = process.env.HOST_IP || '127.0.0.1';
app.set('httpPort', httpPort);
app.set('httpsPort', httpsPort);

var bodyParser = require('body-parser');
var rooms = require('./app/rooms')();

const httpServer = http.createServer((req, res) => {
    res.writeHead(301, { "Location": "https://" + hostIp + ':' + httpsPort + req.url });
    res.end();
});
const httpsServer = https.createServer({
    key: fs.readFileSync('./ssl/key.pem'),
    cert: fs.readFileSync('./ssl/cert.pem')
}, app);

const io = require('socket.io').listen(httpsServer);

app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies

app.use(express.static(path.join(__dirname, 'public')));

app.post('/', (req, res) => {
    const id = req.body.room_id || 0;
    if(id) {
        if(!rooms.find(id)) {
            rooms.create(id);
        }
        res.redirect(`/room/${id}`);
    } else {
        res.redirect('/');
    }
});

app.get('/room/:id', (req, res) => {
    const id = req.params.id;
    if(!rooms.find(id)) {
        res.redirect('/');
    } else {
        res.sendFile('room.html', { root: path.join(__dirname, 'public') });
    }
});

httpServer.listen(httpPort, () => {
    console.log('HTTP @ ' + httpPort);
});
httpsServer.listen(httpsPort, () => {
    console.log('HTTPS @ ' + httpsPort);
});

require('./app/socket')(io, rooms);

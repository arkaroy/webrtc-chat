module.exports = function() {

    var rooms = [];

    var Room = function(id) {
        this.id = id;
        this.status = 'idle';
        this.clients = require('./clients')();
    };

    return {
        create: function(id) {
            var room = new Room(id);
            rooms.push(room);
        },
        find: function(id) {
            var room = rooms.find((element, index, array) => {
                return element.id === id;
            });
            return room ? room : null;
        },
        findIndex: function(id)  {
            var index = rooms.findIndex((element, i, array) => {
                return element.id === id;
            });
            return index;
        },
        get: function(id) {
            return rooms;
        },
        remove: function(id) {
            var index = this.findIndex(id);
            if(index >= 0) {
                rooms.splice(index, 1);
            }
        },
        length: function() {
            return rooms.length;
        }
    };

};
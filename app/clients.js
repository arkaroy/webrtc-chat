module.exports = function() {
    
    var clients = [];
    
    var Client = function(id, name) {
        this.id = id;
        this.name = name || '';
    };
    
    return {
        add: function(id, name) {
            var client = new Client(id, name);
            clients.push(client);
        },
        find: function(id) {
            var client = clients.find((element, index, array) => {
                return element.id === id;
            });
            return client ? client : null;
        },
        findIndex: function(id)  {
            var index = clients.findIndex((element, i, array) => {
                return element.id === id;
            });
            return index;
        },
        remove: function(id) {
            var index = this.findIndex(id);
            if(index >= 0) {
                clients.splice(index, 1);
            }
        },
        get: function() {
            return clients;
        },
        length: function() {
            return clients.length;
        }
    };
    
};
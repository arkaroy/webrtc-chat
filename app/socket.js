module.exports = function(io, rooms) {
    
    io.on('connection', function(socket) {
        
        // Send socket its id
        socket.emit('hello', {
            'now': new Date().getTime(),
            'id': socket.id
        });
        
        // Request to join a room
        socket.on('join', (data) => {
            var roomId = data.roomId;
            if(!rooms.find(roomId)) {
                rooms.create(roomId);
            }
            if(rooms.find(roomId).clients.length() < 10) {
                socket.join('room-' + roomId);
                socket.roomId = roomId;
                socket.emit('join:result', {
                    success: true,
                    others: rooms.find(roomId).clients.get(),
                    status: rooms.find(roomId).status
                });
                rooms.find(roomId).clients.add(data.id, data.name);
                socket.broadcast.to('room-' + roomId).emit('room:join', { client: rooms.find(roomId).clients.find(data.id) });
            } else {
                socket.emit('join:result', { success: false, error: 'Room is full.' });
                socket.disconnect();
            }
        });
        
        socket.on('candidate', (toId, candidate) => {
            io.to(toId).emit('candidate', socket.id, candidate);
        });
        socket.on('signal', (toId, data) => {
            io.to(toId).emit('signal', socket.id, data);
        });
        socket.on('call:initiated', (type) => {
            rooms.find(socket.roomId).status = type;
            socket.broadcast.to('room-' + socket.roomId).emit('call:initiated', socket.id, type);
        });
        socket.on('call:accepted', () => {
            socket.broadcast.to('room-' + socket.roomId).emit('call:accepted', socket.id);
        });
        socket.on('call:hangup', () => {
            rooms.find(socket.roomId).status = 'idle';
            socket.broadcast.to('room-' + socket.roomId).emit('call:hangup');
        });
        function disconnect() {
            var room = 'room-' + socket.roomId;
            socket.broadcast.to(room).emit('room:leave', { id: socket.id });
            socket.leave(room);
            if(rooms.find(socket.roomId) && rooms.find(socket.roomId).clients.find(socket.id)) {
                rooms.find(socket.roomId).clients.remove(socket.id);
            }
            if(rooms.find(socket.roomId) && rooms.find(socket.roomId).clients.length() < 1) {
                rooms.remove(socket.roomId);
            }
        }
        socket.on('disconnect', disconnect);
        socket.on('bye', disconnect);
    });
    
};